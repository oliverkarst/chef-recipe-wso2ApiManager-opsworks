# The base of this example is taken from https://github.com/chef-cookbooks/apt/blob/master/recipes/default.rb

# On systems where apt is not installed, the resources in this recipe are not
# executed. However, they _must_ still be present in the resource collection
# or other cookbooks which notify these resources will fail on non-apt-enabled
# systems.

file '/var/lib/apt/periodic/update-success-stamp' do
  owner 'root'
  group 'root'
  action :nothing
end

# If compile_time_update run apt-get update at compile time
if node['apt']['compile_time_update']
  apt_update('compile time').run_action(:periodic)
end

apt_update 'periodic' do
  only_if { apt_installed? }
end

# For other recipes to call to force an update
execute 'sudo apt-get update' do
  command 'sudo apt-get update'
  ignore_failure true
  action :nothing
  notifies :touch, 'file[/var/lib/apt/periodic/update-success-stamp]', :immediately
  only_if { apt_installed? }
end

# Automatically remove packages that are no longer needed for dependencies
execute 'sudo apt-get -y install unzip' do
  command 'sudo apt-get -y install unzip'
  ignore_failure true
  action :nothing
  notifies :touch, 'file[/var/lib/apt/periodic/update-success-stamp]', :immediately
  only_if { apt_installed? }
end

# Install Java 1. Step: add repository for Java 8
execute 'sudo add-apt-repository ppa:webupd8team/java -y' do
  command 'sudo add-apt-repository ppa:webupd8team/java -y'
  ignore_failure true
  action :nothing
  notifies :touch, 'file[/var/lib/apt/periodic/update-success-stamp]', :immediately
  only_if { apt_installed? }
end

# Reload repository
execute 'sudo apt-get update' do
  command 'sudo apt-get update'
  ignore_failure true
  action :nothing
  notifies :touch, 'file[/var/lib/apt/periodic/update-success-stamp]', :immediately
  only_if { apt_installed? }
end

# Install Java 2. Step: install oracle Java 8
execute 'sudo apt-get install oracle-java8-installer -y' do
  command 'sudo apt-get install oracle-java8-installer -y'
  ignore_failure true
  action :nothing
  notifies :touch, 'file[/var/lib/apt/periodic/update-success-stamp]', :immediately
  only_if { apt_installed? }
end

# Download, unzip and start wso2am
# This steps are taken from the example in http://dataplatforms.blogspot.de/2015/01/downloading-and-running-wso2-api-manager.html
bash 'install_wso2am' do
  user 'root'
  cwd '/opt'
  code <<-EOH
  wget --user-agent="testuser" --referer="http://connect.wso2.com/wso2/getform/reg/new_product_download" http://product-dist.wso2.com/products/api-manager/1.10.0/wso2am-1.10.0.zip
  sudo unzip wso2am-1.10.0.zip
  cd wso2am-1.10.0/bin
  sudo ./wso2server.sh
  EOH
end

# In case when needed this steps can the comment removed
# -------------------------------------------------------------------------
# Automatically remove packages that are no longer needed for dependencies
#execute 'apt-get autoremove' do
#  command 'apt-get -y autoremove'
#  environment(
#    'DEBIAN_FRONTEND' => 'noninteractive'
#  )
#  action :nothing
#  only_if { apt_installed? }
#end

# Automatically remove .deb files for packages no longer on your system
#execute 'apt-get autoclean' do
#  command 'apt-get -y autoclean'
#  action :nothing
#  only_if { apt_installed? }
#end